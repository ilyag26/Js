"use strict";


function buttoncreate() {
    var table = document.createElement('table');

    table.id = 'table';
    table.cellPadding = '20';
    table.border = '1';

    var tr = document.createElement('tr');
    var tr1 = document.createElement('tr');
    var tr2 = document.createElement('tr');
    var tr3 = document.createElement('tr');
    var tr4 = document.createElement('tr');
    var tr5 = document.createElement('tr');
    table.appendChild(tr);
    table.appendChild(tr1);
    table.appendChild(tr2);
    table.appendChild(tr3);
    table.appendChild(tr4);
    table.appendChild(tr5);
    tr.innerHTML = "<th id='1' ></th>" + "<th id='3'></th>" + "<th id='1-1'></th>" + "<th id='1-3'></th>" + "<th id='2-1'></th>" + "<th id='2-2'></th>";
    tr1.innerHTML = "<td id='2'></td>" + "<td id='4'></td>" + "<td id='1-2'></td>" + "<td id='1-4'></td>" + "<td id='2-3'></td>" + "<td id='2-4'></td>";
    tr2.innerHTML = "<td id='3-1'></td>" + "<td id='3-3'></td>" + "<td id='4-1'></td>" + "<td id='4-3'></td>" + "<td id='3-5'></td>" + "<td id='3-7'></td>";
    tr3.innerHTML = "<td id='3-2'></td>" + "<td id='3-4'></td>" + "<td id='4-2'></td>" + "<td id='4-4'></td>" + "<td id='3-6'></td>" + "<td id='3-8'></td>";
    tr4.innerHTML = "<td id='2-5'></td>" + "<td id='2-7'></td>" + "<td id='1-5'></td>" + "<td id='1-7'></td>" + "<td id='5'></td>" + "<td id='7'></td>";
    tr5.innerHTML = "<td id='2-6'></td>" + "<td id='2-8'></td>" + "<td id='1-6'></td>" + "<td id='1-8'></td>" + "<td id='6'></td>" + "<td id='8'></td>";
    document.getElementById("div").appendChild(table);
    var randcolbutton2 = "";
    var allcharbutton2 = "0123456789ABCDEF";
    for (var button2= 0; button2 < 6; button2++) {
        randcolbutton2 += allcharbutton2[Math.floor(Math.random() * 16)];
    }
    document.getElementById("button2").style.borderColor = "#" + randcolbutton2;
}


function change() {
    var randcol = "";
    var allchar = "0123456789ABCDEF";
    for (var b = 0; b < 6; b++) {
        randcol += allchar[Math.floor(Math.random() * 16)];
    }

    //change color in 1,2,7,8,29,30,35,36
    document.getElementById("1").style.backgroundColor = "#" + randcol;
    document.getElementById("2").style.backgroundColor = "#" + randcol;
    document.getElementById("3").style.backgroundColor = "#" + randcol;
    document.getElementById("4").style.backgroundColor = "#" + randcol;
    document.getElementById("5").style.backgroundColor = "#" + randcol;
    document.getElementById("6").style.backgroundColor = "#" + randcol;
    document.getElementById("7").style.backgroundColor = "#" + randcol;
    document.getElementById("8").style.backgroundColor = "#" + randcol;
    //change color in 3,4,9,10,27,28,33,34
    var randcol1 = "";
    var allchar1 = "0123456789ABCDEF";
    for (var b1 = 0; b1 < 6; b1++) {
        randcol1 += allchar1[Math.floor(Math.random() * 16)];
    }
    document.getElementById("1-1").style.backgroundColor = "#" + randcol1;
    document.getElementById("1-2").style.backgroundColor = "#" + randcol1;
    document.getElementById("1-3").style.backgroundColor = "#" + randcol1;
    document.getElementById("1-4").style.backgroundColor = "#" + randcol1;
    document.getElementById("1-5").style.backgroundColor = "#" + randcol1;
    document.getElementById("1-6").style.backgroundColor = "#" + randcol1;
    document.getElementById("1-7").style.backgroundColor = "#" + randcol1;
    document.getElementById("1-8").style.backgroundColor = "#" + randcol1;
    //change color in 5,6,11,12,25,26,31,32
    var randcol2 = "";
    var allchar2 = "0123456789ABCDEF";
    for (var b2 = 0; b2 < 6; b2++) {
        randcol2 += allchar2[Math.floor(Math.random() * 16)];
    }
    document.getElementById("2-1").style.backgroundColor = "#" + randcol2;
    document.getElementById("2-2").style.backgroundColor = "#" + randcol2;
    document.getElementById("2-3").style.backgroundColor = "#" + randcol2;
    document.getElementById("2-4").style.backgroundColor = "#" + randcol2;
    document.getElementById("2-5").style.backgroundColor = "#" + randcol2;
    document.getElementById("2-6").style.backgroundColor = "#" + randcol2;
    document.getElementById("2-7").style.backgroundColor = "#" + randcol2;
    document.getElementById("2-8").style.backgroundColor = "#" + randcol2;
    //change color in 13,14,19,20,17,18,23,24
    var randcol3 = "";
    var allchar3 = "0123456789ABCDEF";
    for (var b3 = 0; b3 < 6; b3++) {
        randcol3 += allchar3[Math.floor(Math.random() * 16)];
    }
    document.getElementById("3-1").style.backgroundColor = "#" + randcol3;
    document.getElementById("3-2").style.backgroundColor = "#" + randcol3;
    document.getElementById("3-3").style.backgroundColor = "#" + randcol3;
    document.getElementById("3-4").style.backgroundColor = "#" + randcol3;
    document.getElementById("3-5").style.backgroundColor = "#" + randcol3;
    document.getElementById("3-6").style.backgroundColor = "#" + randcol3;
    document.getElementById("3-7").style.backgroundColor = "#" + randcol3;
    document.getElementById("3-8").style.backgroundColor = "#" + randcol3;
    //change color in 15,16,21,22
    var randcol4 = "";
    var allchar4 = "0123456789ABCDEF";
    for (var b4 = 0; b4 < 6; b4++) {
        randcol4 += allchar4[Math.floor(Math.random() * 16)];
    }
    document.getElementById("4-1").style.backgroundColor = "#" + randcol4;
    document.getElementById("4-2").style.backgroundColor = "#" + randcol4;
    document.getElementById("4-3").style.backgroundColor = "#" + randcol4;
    document.getElementById("4-4").style.backgroundColor = "#" + randcol4;

    var randcolborder = "";
    var allcharborder = "0123456789ABCDEF";
    for (var border = 0; border < 6; border++) {
        randcolborder += allcharborder[Math.floor(Math.random() * 16)];
    }
    document.getElementById("border").style.borderColor = "#" + randcolborder;
    var randcolbutton = "";
    var allcharbutton = "0123456789ABCDEF";
    for (var button = 0; button < 6; button++) {
        randcolbutton += allcharbutton[Math.floor(Math.random() * 16)];
    }
    document.getElementById("button").style.borderColor = "#" + randcolbutton;

}
